const expect = require('expect');

const utils = require('./utils.js');

describe('Utils', () => {
	describe('#add', () => {
		it('should add two numbers', () => {
			var res = utils.add(33, 11);
			expect(res).toBeA('number').toBe(44);
		});
	});
	
	describe('#asyncAdd', () => {
		it('should async add two numbers', (done) => {
			utils.asyncAdd(4, 3, (sum) => {
				expect(sum).toBe(7).toBeA('number');
				done();
			});
		});
	});

	describe('#square', () => {
		it('should give square of number', () => {
			var res = utils.square(9);
			expect(res).toBeA('number').toBe(81);
		});
	});

	describe('#asyncSquare', () => {
		it('should give async square of number', (done) => {
			utils.asyncSquare(9, (square) => {
				expect(square).toBe(81).toBeA('number');
				done();
			});
		});
	});
});

//it('should expect some values', () => {
////	expect({name: 'millo'}).toBe({name: 'millo'}); //throws error because 'toBe' checks for exact equal and expect and toBe have seperate objects.
////	expect({name: 'millo'}).toEqual({name: 'Millo'});
//	expect({name: 'millo'}).toNotEqual({name: 'Millo'});
//	expect([1, 2, 3, 4]).toInclude(2);
//	expect([1, 2, 3, 4]).toNotInclude(5);
//	expect({
//		name: 'millo',
//		age: 33
//	}).toInclude({
//		age: 33
//	});
//});

//it('first name and last name should be set', () => {
//	var user = {};
//	var names = utils.setName(user, 'Millo Lailang');
//	expect(names).toInclude({
//		firstName: 'Millo',
//		lastName: 'Lailang'
//	});
//});
