const express = require('express');

var app = express();

app.get('/', (req, res) => {
	//res.send('Hello World!');
	res.status(404).send({
		error: 'Page not found',
		name: 'Todo App v1.0'
	});
});

app.get('/users', (req, res) => {
	res.send([
		{
			name: 'Millo Lailang',
			age: 32
		},
		{
			name: 'Millo Chilyang',
			age: 28
		},
		{
			name: 'Millo Nopo',
			age: 25
		}
	]);
});

app.listen(3000, () => console.log('app listening on port 3000'));

module.exports.app = app;
